CREATE TABLE travel(
    id INTEGER PRIMARY KEY,
    name varchar(255) NOT NULL UNIQUE,
    days INTEGER NOT NULL
);