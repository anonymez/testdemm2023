from __future__ import print_function,annotations
import os
from flask import Flask, request
import mysql.connector

def get_db_cursor():
    mydb = mysql.connector.connect(
        host=os.getenv("DB_HOST","localhost"),
        user=os.getenv("DB_USER","user"),
        password=os.getenv("DB_PASSWORD","password"),
        database=os.getenv("DB_NAME","travel"),
    )
    return mydb


app = Flask(__name__)
@app.route('/')
def hello_world():
    return "Hello World 2023"

@app.route('/studenti')
def hello_world_studenti():
    return "Hello World Studenti from 2023!"

@app.route('/travels',methods=["GET","POST"])
def all_travels():
    if request.method == "GET":
        # method:GET,url:/travels, header: Application-type: json
        query="SELECT * FROM travel"
        conn=get_db_cursor()
        cur=conn.cursor()
        travels=[]
        cur.execute(query)
        for (id,name,days) in cur:
            travels.append({"id":id,"name":name,"days":days})
        cur.close()
        conn.close()
        return {"all_travels":travels}
        #return "You are asking all available travels"
    if request.method == "POST":
        # method:POST,url:/travels, header: Application-type: json, Body: {"id":22,"name":"viaggio alle maldive","days":12}
        if request.is_json:
            request_body=request.get_json()
            t_id=request_body.get("id")
            t_name=request_body.get("name")
            t_days=request_body.get("days")
        #START DATABASE PART
        query="INSERT INTO travel (id,name,days) VALUES ("+t_id+","+t_name+","+t_days+")"
              #INSERT INTO travel (id,name,days) VALUES (22,viaggio alle maldive,12)
        conn=get_db_cursor()
        cur=conn.cursor()
        cur.execute(query)
        conn.commit()
        cur.close()
        conn.close()
        #END DATABASE PART
        return "",201

@app.route('/travels/<id>',methods=["GET","PUT","DELETE"])
def single_travel(id):
    if request.method == "GET":
        return "You are asking to read travel with id: "+id
    if request.method == "PUT":
        return "You are asking to update travel with id: "+id
    if request.method == "DELETE":
        return "You are asking to delete travel with id: "+id


if __name__ == "__main__":
     port = os.getenv("MY_PORT",5002)
     app.run(host="0.0.0.0",port=port)



