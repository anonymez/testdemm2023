#immagine di partenza
FROM python:3.9.16
#aggiunta variabile env MY_PORT
ENV MY_PORT=80
#copio da filesystem docker host a immagine un file
COPY main.py /opt/init.py
#copio come sopra i requirements per dipendenze
ADD requirements.txt /opt/requirements.txt
#installo dipendenze
RUN pip install -r /opt/requirements.txt
#comando da eseguire alla docker run
CMD python /opt/init.py